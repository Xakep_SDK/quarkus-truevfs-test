package dk.xakeps;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;

@Path("/hello")
public class GreetingResource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() throws IOException {
        // Does not work in production - it can't find provider as if it's not present in a classpath
        // Does not work in tests:
        // Caused by: java.util.ServiceConfigurationError:
        //     net.java.truevfs.kernel.spec.spi.FsManagerFactory:
        //         net.java.truevfs.kernel.impl.DefaultManagerFactory not a subtype
//        try (FileSystem fileSystem = FileSystems.newFileSystem(Paths.get("test.tar.gz"))) {
//            return String.join("\n", Files.readAllLines(fileSystem.getPath("/test.txt")));
//        }

        // Does work in production
        // Does not work in tests:
        // Caused by: java.util.ServiceConfigurationError:
        //     net.java.truevfs.kernel.spec.spi.FsManagerFactory:
        //         net.java.truevfs.kernel.impl.DefaultManagerFactory not a subtype
        try (FileSystem fileSystem = FileSystems.newFileSystem(Paths.get("test.tar.gz"), getClass().getClassLoader())) {
            return String.join("\n", Files.readAllLines(fileSystem.getPath("/test.txt")));
        }
    }
}